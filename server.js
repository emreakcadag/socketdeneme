const http = require('http');
const socketio = require('socket.io');
const server = http.createServer((req, res) => {
    res.end('Selam!');
});

server.listen(3000);

const io = socketio.listen(server);

io.sockets.on('connection', (socket) => {
    console.log('Client connected.');

    socket.on('selam ver', (data) => {
        console.log(data.name);
        console.log(data.surname);
    });

    socket.on('disconnect', () => {
        console.log('Client left.');
    });
});